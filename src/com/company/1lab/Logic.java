package com.company.1lab;

import javax.swing.*;//необходимо для формочек(лейбл, надпись)
import java.awt.*;//импорт из библиотеки всех подклассов и подфункций
import java.awt.event.*;//отслеживание нажатий на кнопку

class Logic{
    public boolean prime(int number){//проверка на простоту
        if(number == 2) return true;
        if( number == 0 || number == 1 || number % 2 == 0) return false;
        for(int i = 3; i * i <= number; i +=2){
            if (number % i == 0) return false;
        }
        return true;
    }
    public boolean even(int number){//проверка на четность
        if (number % 2 == 0) return true;
        else return false;
    }
}
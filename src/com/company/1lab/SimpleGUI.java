package com.company.1lab;
import javax.swing.*;//необходимо для формочек(лейбл, надпись)
import java.awt.*;//импорт из библиотеки всех подклассов и подфункций
import java.awt.event.*;//отслеживание нажатий на кнопку

public class SimpleGUI extends JFrame {//наследование от класса, который может создавать формочки
    private JButton button = new JButton("Проверить");
    private JTextField input = new JTextField("", 5);
    private JLabel label = new JLabel("Число: ");
    private JLabel label1 = new JLabel("Простое?");
    private JLabel label2 = new JLabel("Четное?");

    public SimpleGUI() {
        this.setBounds(300, 300, 250, 150);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container container = this.getContentPane();
        container.setLayout(new GridLayout(4, 4, 3, 2));
        container.add(label);
        container.add(input);

        container.add(label1);
        container.add(label2);
        String text = input.getText();
        button.addActionListener(new com.company.SimpleGUI.ButtonEventListener());
        //работаем с кнопкой. AddActionListener вызывает класс, в котором будет реализация действия
        container.add(button);
    }
    class ButtonEventListener implements ActionListener {//реализует интерфейс
        public ButtonEventListener(){
        }

        public void actionPerformed(ActionEvent e) {//какое-то событие передается в качестве параметра
            Logic logic = new Logic();
            String message1 = logic.prime(Integer.parseInt(input.getText())) ? "да" : "нет";
            String message2 = logic.even(Integer.parseInt(input.getText())) ? "да" : "нет";
            label1.setText("Простое? " + message1);
            label2.setText("Четное? " + message2);
        }
    }
}
